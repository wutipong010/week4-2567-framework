//Define Object
const users = [
    {
        fname: 'Mr.Mark Zuckerburg',
        address: '15th Park Avenue',
        age: 45
    },
    {
        fname: 'Mr.Pelakorn Penukao',
        address: 'Trat',
        age: 40
    },
    {
        fname: 'Mr.Bill Gate',
        address: 'London',
        age: 35
    }
];

const drinks = ["Coke", "Pepsi", "Fanta", "Orange"];
for (const drink in drinks){
    console.log(`${drink}: ${drinks[drink]}`)
}

const fname = users[0].fname
const fname1 = users[1].fname
console.log(fname,fname1)

// Object Destructuring with for of loop
for (const user of users){
    console.log(`Name:${user.fname} Address:${user.address} Age:${user.age} Salary:${user.salary}`)
}